﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed; //variable for speed
    private Rigidbody2D rb2d; //reference for the rigid body object
    private Vector2 origin; //reference for origin point's location
    private bool controlSwitch;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        origin = new Vector2(0, 0); // sets reference to center of scene
        controlSwitch = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (controlSwitch == true)
        {
            float moveHorizontal = Input.GetAxis("Horizontal"); //moves ship left and right

            float moveVertical = Input.GetAxis("Vertical"); //move ship up and down

            Vector2 movement = new Vector2(moveHorizontal, moveVertical);

            rb2d.AddForce(movement * speed);
        }

	    if (Input.GetKey(KeyCode.Escape)) //Hit escape to close the program
	    {
            Application.Quit();
            Debug.Log("Application closed");
	    }

        if(Input.GetKey(KeyCode.Space)) //hit space to reset ship position
        {
            rb2d.MovePosition(origin);
        }

        if(Input.GetKey(KeyCode.P)) //P will disable movement and reenable it
        {
            if(controlSwitch == true)
            {
                controlSwitch = false;
                Debug.Log("Controls off");
            }
            else
            {
                controlSwitch = true;
                Debug.Log("Controls on");
            }
        }

       // if(Input.GetKey(KeyCode.Q)) //Q sets ship's gameObject as inactive (broken)
        {
            //GameObject.SetActive(false);
        }
	}
}
